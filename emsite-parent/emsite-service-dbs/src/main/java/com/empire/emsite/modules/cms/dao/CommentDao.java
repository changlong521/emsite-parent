/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.dao;

import com.empire.emsite.common.persistence.CrudDao;
import com.empire.emsite.common.persistence.annotation.MyBatisDao;
import com.empire.emsite.modules.cms.entity.Comment;

/**
 * 类CommentDao.java的实现描述：评论DAO接口
 * 
 * @author arron 2017年10月30日 下午4:07:42
 */
@MyBatisDao
public interface CommentDao extends CrudDao<Comment> {

}
