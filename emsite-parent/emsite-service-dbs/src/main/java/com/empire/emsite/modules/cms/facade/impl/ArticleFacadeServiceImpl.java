/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.cms.entity.Article;
import com.empire.emsite.modules.cms.facade.ArticleFacadeService;
import com.empire.emsite.modules.cms.service.ArticleService;

/**
 * 类ArticleFacadeServiceImpl.java的实现描述：文章FacadeService接口实现类
 * 
 * @author arron 2017年9月17日 下午9:44:55
 */
@Service
public class ArticleFacadeServiceImpl implements ArticleFacadeService {
    @Autowired
    private ArticleService articleService;

    @Override
    public Article get(String id) {
        return articleService.get(id);
    }

    @Override
    public Page<Article> findPage(Page<Article> page, Article article, boolean isDataScopeFilter) {
        return articleService.findPage(page, article, isDataScopeFilter);
    }

    @Override
    public void save(Article article) {
        articleService.save(article);
    }

    @Override
    public void delete(Article article, Boolean isRe) {
        articleService.delete(article, isRe);
    }

    /**
     * 通过编号获取内容标题
     * 
     * @return new Object[]{栏目Id,文章Id,文章标题}
     */
    @Override
    public List<Object[]> findByIds(String ids) {
        return articleService.findByIds(ids);
    }

    /**
     * 点击数加一
     */
    @Override
    public void updateHitsAddOne(String id) {
        articleService.updateHitsAddOne(id);
    }

    /**
     * 更新索引
     */
    @Override
    public void createIndex() {
        articleService.createIndex();
    }

    /**
     * 全文检索
     */
    //FIXME 暂不提供检索功能
    @Override
    public Page<Article> search(Page<Article> page, String q, String categoryId, String beginDate, String endDate) {
        return articleService.search(page, q, categoryId, beginDate, endDate);
    }

}
