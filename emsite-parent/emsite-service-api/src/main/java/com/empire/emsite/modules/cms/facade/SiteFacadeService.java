/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.cms.entity.Site;

/**
 * 类SiteFacadeService.java的实现描述：站点FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:44:16
 */
public interface SiteFacadeService {
    public Site get(String id);

    public Page<Site> findPage(Page<Site> page, Site site);

    public void save(Site site);

    public void delete(Site site, Boolean isRe);
}
