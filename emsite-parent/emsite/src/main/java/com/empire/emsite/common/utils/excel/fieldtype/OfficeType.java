/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.utils.excel.fieldtype;

import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.modules.sys.entity.Office;
import com.empire.emsite.modules.sys.utils.UserUtils;

/**
 * 类OfficeType.java的实现描述：字段类型转换
 * 
 * @author arron 2017年10月30日 下午7:05:22
 */
public class OfficeType {

    /**
     * 获取对象值（导入）
     */
    public static Object getValue(String val) {
        for (Office e : UserUtils.getOfficeList()) {
            if (StringUtils.trimToEmpty(val).equals(e.getName())) {
                return e;
            }
        }
        return null;
    }

    /**
     * 设置对象值（导出）
     */
    public static String setValue(Object val) {
        if (val != null && ((Office) val).getName() != null) {
            return ((Office) val).getName();
        }
        return "";
    }
}
