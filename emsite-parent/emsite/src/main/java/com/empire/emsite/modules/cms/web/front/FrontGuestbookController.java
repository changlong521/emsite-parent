/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.web.front;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empire.emsite.common.config.MainConfManager;
import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.cms.entity.Guestbook;
import com.empire.emsite.modules.cms.entity.Site;
import com.empire.emsite.modules.cms.facade.GuestbookFacadeService;
import com.empire.emsite.modules.cms.utils.CmsUtils;
import com.empire.emsite.modules.sys.utils.UserUtils;

/**
 * 类FrontGuestbookController.java的实现描述：留言板Controller
 * 
 * @author arron 2017年10月30日 下午7:14:32
 */
@Controller
@RequestMapping(value = "${frontPath}/guestbook")
public class FrontGuestbookController extends BaseController {

    @Autowired
    private GuestbookFacadeService guestbookFacadeService;

    /**
     * 留言板
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String guestbook(@RequestParam(required = false, defaultValue = "1") Integer pageNo,
                            @RequestParam(required = false, defaultValue = "30") Integer pageSize, Model model) {
        Site site = CmsUtils.getSite(Site.defaultSiteId());
        model.addAttribute("site", site);

        Page<Guestbook> page = new Page<Guestbook>(pageNo, pageSize);
        Guestbook guestbook = new Guestbook();
        guestbook.setDelFlag(Guestbook.DEL_FLAG_NORMAL);
        guestbook.setCurrentUser(UserUtils.getUser());
        page = guestbookFacadeService.findPage(page, guestbook);
        model.addAttribute("page", page);
        return "modules/cms/front/themes/" + site.getTheme() + "/frontGuestbook";
    }

    /**
     * 留言板-保存留言信息
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public String guestbookSave(Guestbook guestbook, String validateCode, HttpServletRequest request,
                                HttpServletResponse response, RedirectAttributes redirectAttributes) {
        //		if (StringUtils.isNotBlank(validateCode)){
        //			if (ValidateCodeServlet.validate(request, validateCode)){
        guestbook.setIp(request.getRemoteAddr());
        guestbook.setCreateDate(new Date());
        guestbook.setDelFlag(Guestbook.DEL_FLAG_AUDIT);
        guestbook.setCurrentUser(UserUtils.getUser());
        guestbookFacadeService.save(guestbook);
        addMessage(redirectAttributes, "提交成功，谢谢！");
        //			}else{
        //				addMessage(redirectAttributes, "验证码不正确。");
        //			}
        //		}else{
        //			addMessage(redirectAttributes, "验证码不能为空。");
        //		}
        return "redirect:" + MainConfManager.getFrontPath() + "/guestbook";
    }

}
