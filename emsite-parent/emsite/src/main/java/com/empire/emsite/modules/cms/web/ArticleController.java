/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empire.emsite.common.mapper.JsonMapper;
import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.cms.entity.Article;
import com.empire.emsite.modules.cms.entity.Category;
import com.empire.emsite.modules.cms.facade.ArticleDataFacadeService;
import com.empire.emsite.modules.cms.facade.ArticleFacadeService;
import com.empire.emsite.modules.cms.facade.CategoryFacadeService;
import com.empire.emsite.modules.cms.facade.SiteFacadeService;
import com.empire.emsite.modules.cms.service.FileTplService;
import com.empire.emsite.modules.cms.utils.CmsUtils;
import com.empire.emsite.modules.cms.utils.TplUtils;
import com.empire.emsite.modules.sys.utils.UserUtils;

/**
 * 类ArticleController.java的实现描述：文章Controller
 * 
 * @author arron 2017年10月30日 下午7:10:21
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/article")
public class ArticleController extends BaseController {

    @Autowired
    private ArticleFacadeService     articleFacadeService;
    @Autowired
    private ArticleDataFacadeService articleDataFacadeService;
    @Autowired
    private CategoryFacadeService    categoryFacadeService;
    @Autowired
    private FileTplService           fileTplService;
    @Autowired
    private SiteFacadeService        siteFacadeService;

    @ModelAttribute
    public Article get(@RequestParam(required = false) String id) {
        if (StringUtils.isNotBlank(id)) {
            return articleFacadeService.get(id);
        } else {
            return new Article();
        }
    }

    @RequiresPermissions("cms:article:view")
    @RequestMapping(value = { "list", "" })
    public String list(Article article, HttpServletRequest request, HttpServletResponse response, Model model) {
        //		for (int i=0; i<10000000; i++){
        //			Article a = new Article();
        //			a.setCategory(new Category(article.getCategory().getId()));
        //			a.setTitle("测试测试测试测试测试测试测试测试"+a.getCategory().getId());
        //			a.setArticleData(new ArticleData());
        //			a.getArticleData().setContent(a.getTitle());
        //			articleService.save(a);
        //		}
        Page<Article> page = articleFacadeService.findPage(new Page<Article>(request, response), article, true);
        model.addAttribute("page", page);
        return "modules/cms/articleList";
    }

    @RequiresPermissions("cms:article:view")
    @RequestMapping(value = "form")
    public String form(Article article, Model model) {
        // 如果当前传参有子节点，则选择取消传参选择
        if (article.getCategory() != null && StringUtils.isNotBlank(article.getCategory().getId())) {
            List<Category> list = categoryFacadeService.findByParentId(article.getCategory().getId(),
                    CmsUtils.getCurrentSiteId());
            if (list.size() > 0) {
                article.setCategory(null);
            } else {
                article.setCategory(categoryFacadeService.get(article.getCategory().getId()));
            }
        }
        article.setArticleData(articleDataFacadeService.get(article.getId()));
        //		if (article.getCategory()=null && StringUtils.isNotBlank(article.getCategory().getId())){
        //			Category category = categoryService.get(article.getCategory().getId());
        //		}
        model.addAttribute("contentViewList", getTplContent());
        model.addAttribute("article_DEFAULT_TEMPLATE", Article.DEFAULT_TEMPLATE);
        model.addAttribute("article", article);
        CmsUtils.addViewConfigAttribute(model, article.getCategory());
        return "modules/cms/articleForm";
    }

    @RequiresPermissions("cms:article:edit")
    @RequestMapping(value = "save")
    public String save(Article article, Model model, RedirectAttributes redirectAttributes) {
        if (!beanValidator(model, article)) {
            return form(article, model);
        }
        article.setCurrentUser(UserUtils.getUser());
        if (!UserUtils.getSubject().isPermitted("cms:article:audit")) {
            article.setDelFlag(Article.DEL_FLAG_AUDIT);
        }
        articleFacadeService.save(article);
        addMessage(redirectAttributes, "保存文章'" + StringUtils.abbr(article.getTitle(), 50) + "'成功");
        String categoryId = article.getCategory() != null ? article.getCategory().getId() : null;
        return "redirect:" + adminPath + "/cms/article/?repage&category.id=" + (categoryId != null ? categoryId : "");
    }

    @RequiresPermissions("cms:article:edit")
    @RequestMapping(value = "delete")
    public String delete(Article article, String categoryId, @RequestParam(required = false) Boolean isRe,
                         RedirectAttributes redirectAttributes) {
        // 如果没有审核权限，则不允许删除或发布。
        if (!UserUtils.getSubject().isPermitted("cms:article:audit")) {
            addMessage(redirectAttributes, "你没有删除或发布权限");
        }
        articleFacadeService.delete(article, isRe);
        addMessage(redirectAttributes, (isRe != null && isRe ? "发布" : "删除") + "文章成功");
        return "redirect:" + adminPath + "/cms/article/?repage&category.id=" + (categoryId != null ? categoryId : "");
    }

    /**
     * 文章选择列表
     */
    @RequiresPermissions("cms:article:view")
    @RequestMapping(value = "selectList")
    public String selectList(Article article, HttpServletRequest request, HttpServletResponse response, Model model) {
        list(article, request, response, model);
        return "modules/cms/articleSelectList";
    }

    /**
     * 通过编号获取文章标题
     */
    @RequiresPermissions("cms:article:view")
    @ResponseBody
    @RequestMapping(value = "findByIds")
    public String findByIds(String ids) {
        List<Object[]> list = articleFacadeService.findByIds(ids);
        return JsonMapper.nonDefaultMapper().toJson(list);
    }

    private List<String> getTplContent() {
        List<String> tplList = fileTplService.getNameListByPrefix(siteFacadeService.get(CmsUtils.getCurrentSiteId())
                .getSolutionPath());
        tplList = TplUtils.tplTrim(tplList, Article.DEFAULT_TEMPLATE, "");
        return tplList;
    }
}
